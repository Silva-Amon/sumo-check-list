import Checklist from '../Checklist'
import SacramentalListData from './SacramentalListData'

const {
    duranteSacramental,
    aposSacramental,
    aposAulasClasses,
    depoisReuniaoDominical
} = SacramentalListData
const SacramentalList = () =>  {
    return (
        <>
            <h2 id='domingo'>Coisas a fazer no Domingo</h2>
            <Checklist
                title="Durante a Sacramental"
                checkListName="duranteSacramental"
                checklistDefault={duranteSacramental} 
            />
            <Checklist
                title="Após a Sacramental"
                checkListName="aposSacramental"
                checklistDefault={aposSacramental} 
            />
            <Checklist
                title="Após as Aulas das Classes"
                checkListName="aposAulasClasses"
                checklistDefault={aposAulasClasses} 
            />
            <Checklist
                title="Depois da reunião Dominical"
                checkListName="depoisReuniaoDominical"
                checklistDefault={depoisReuniaoDominical} 
            />
            <br />
            <a href='https://docs.google.com/forms/d/1KwKRaifnC8BEgdnYx1wFOvgd_nbYoLssl6v30aJaJaw/edit#responses'><strong>Link formulário de acompanhamento</strong></a>
            <br />
            <br />
            <hr />
        </>
    );
}
export default SacramentalList;