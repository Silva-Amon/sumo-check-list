import { useState, useEffect } from 'react';
import {
  Checkbox,
  List,
  ListItem,
  ListItemText,
  Container,
  TextField,
  Button,
  IconButton,
} from '@mui/material';

import DeleteIcon from '@mui/icons-material/Delete';
import { css } from '@emotion/css';

const Checklist = ({title, checkListName, checklistDefault}) => {
  const checkListSaved = JSON.parse(localStorage.getItem(checkListName))
  const savedItems = checkListSaved || checklistDefault || [];
  const [items, setItems] = useState(savedItems);
  const [newItem, setNewItems] = useState();
  
  // Update local storage when items change
  useEffect(() => {
    localStorage.setItem(checkListName, JSON.stringify(items), 8640);
  }, [items, checkListName]);

  const handleToggle = (index) => {
    const newCheckedItems = [...items];
    newCheckedItems[index]["checked"] = !newCheckedItems[index]["checked"];
    setItems(newCheckedItems);
  };

  const addItem = (text) => {
    if (!text) return
    const item = { text, checked: false, canDelete: true };
    setItems([...items, item]);
    setNewItems("")
  };

  const removeItem = (e, index) => {
    const currItems = [...items];
    currItems.splice(index, 1);
    setItems(currItems);
    e.stopPropagation()
  };

  return (
    <Container>
      <h3>{title}</h3>

      <List>
        {items.map((item, index) => (
          <>
            <ListItem
              key={index}
              dense
              button
              onClick={() => handleToggle(index)}
            >
              <Checkbox checked={items[index]["checked"] || false} />
              <ListItemText sx={items[index]["checked"] &&  { textDecoration: 'line-through'}} primary={item.text} />
              <IconButton
                aria-label="deletar"
                onClick={(e)=> removeItem(e, index)}
                color="error"
                disabled={!items[index]["canDelete"] ?? false}
              >
                <DeleteIcon />
              </IconButton>
            </ListItem>
          </>
        ))}
      </List>

      <h4>Adicione itens ao checklist acima:</h4>
      <TextField
        id="standard-basic"
        label="Devo fazer..."
        variant="standard"
        onChange={(e)=>setNewItems(e.target.value)}
        onKeyDown={(e) => {
          if (e.key === 'Enter') {
            addItem(newItem);
          }
        }}
        value={newItem}
      />
      <br />
      <br />
      <Button
        variant="contained"
        onClick={()=> addItem(newItem)}
      >
        Adicionar ao checklist
      </Button>
      <br />
      <br />
      <hr />
    </Container>
  );
};

export default Checklist;