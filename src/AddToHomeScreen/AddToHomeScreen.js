import { useAddToHomescreenPrompt } from "./AddToHomeScreenAct"
import Button from '@mui/material/Button';

const AddToHomeScreen = () => {
    const [prompt, promptToInstall] = useAddToHomescreenPrompt();

    return (
        <Button
            variant="contained"
            onClick={promptToInstall}
            disabled={!prompt}
        >
            {!prompt ? 'App já instalado' : 'Instalar App'}
        </Button>
    )
}

export default AddToHomeScreen;