import Checklist from '../Checklist'
import ConselMeetingListData from './ConselMeetingListData'

const {
    duranteReuniaoConselho,
    aposReuniaoConselho,
} = ConselMeetingListData
const ConselMeetingList = () =>  {
    return (
        <>
            <h2 id='conselho'>Coisas a fazer no Conselho da Ala</h2>
            <Checklist
                title="Durante o conselho"
                checkListName="duranteReuniaoConselho"
                checklistDefault={duranteReuniaoConselho} 
            />
            <Checklist
                title="Após reuniao"
                checkListName="aposConselho"
                checklistDefault={aposReuniaoConselho} 
            />
            <br />
            <a href='https://forms.office.com/r/B01fer27Qc'><strong>Link formulário de acompanhamento do Conselho da Ala</strong></a>
            <br />
            <br />
            <hr />
        </>
    );
}
export default ConselMeetingList;