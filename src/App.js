import SacramentalList from './SacramentalList'
import ConselMeetingList from './ConselMeetingList'
import AddToHomeScreen from './AddToHomeScreen'
import { Container } from '@mui/material';

function App() {

  return (
    <Container>
      <h1>Checklist do Sumo</h1>
      <AddToHomeScreen />
      <br />
      <br />
      <h2>Links Rápidos</h2>
      <a href='#domingo'>Coisas a fazer no domingo</a>
      <br />
      <br />
      <a href='#conselho'>Coisas a fazer no conselho da ala</a>
      <br />
      <br />
      <SacramentalList />
      <ConselMeetingList />
    </Container>
  );
}

export default App;
